package com.digitalml.healthcare.financial.claim;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for ClaimAmountSet:
{
  "type": "object",
  "properties": {
    "billedChargeAmount": {
      "$ref": "Amount"
    },
    "coinsuranceAmount": {
      "$ref": "Amount"
    },
    "copaymentAmount": {
      "$ref": "Amount"
    },
    "deductibleAmount": {
      "$ref": "Amount"
    },
    "nonCoveredAmount": {
      "$ref": "Amount"
    },
    "allowedAmount": {
      "$ref": "Amount"
    },
    "withholdingAmount": {
      "$ref": "Amount"
    },
    "paidAmount": {
      "$ref": "Amount"
    },
    "HRAPaidAmount": {
      "$ref": "Amount"
    },
    "HRAAppliedAmount": {
      "$ref": "Amount"
    },
    "CDHPDeductiblePriorToHRAAmount": {
      "$ref": "Amount"
    },
    "memberResponsibilityAmount": {
      "$ref": "Amount"
    },
    "memberLiabilityAmount": {
      "$ref": "Amount"
    },
    "memberNotResponsibilityAmount": {
      "$ref": "Amount"
    },
    "providerResponsibilityAmount": {
      "$ref": "Amount"
    },
    "nonCoveredProviderResponsibilityAmount": {
      "$ref": "Amount"
    },
    "providerWriteOffAmount": {
      "$ref": "Amount"
    },
    "otherCarrierAmount": {
      "$ref": "Amount"
    },
    "planSavingsAmount": {
      "$ref": "Amount"
    },
    "UNCAmount": {
      "$ref": "Amount"
    },
    "salesTaxAmount": {
      "$ref": "Amount"
    },
    "dispensingFeeAmount": {
      "$ref": "Amount"
    },
    "ingredientCostAmount": {
      "$ref": "Amount"
    },
    "pharmacyAncillaryAmount": {
      "$ref": "Amount"
    },
    "discountAmount": {
      "description": "TOTAL COORDINATION OF BENEFITS OTHER CARRIER COINSURANCE AMOUNT ",
      "$ref": "Amount"
    },
    "carrierExclusionAmount": {
      "description": "OTHER CARRIER CONTRACT EXCLUSION AMOUNT ",
      "$ref": "Amount"
    },
    "totalAllowedAmount": {
      "$ref": "Amount"
    },
    "carrierLiabilityAmount": {
      "description": "OTHER CARRIER CONTRACT LIABILITY AMOUNT ",
      "$ref": "Amount"
    },
    "totalBilledChargeAmount": {
      "$ref": "Amount"
    },
    "carrierLimitAmount": {
      "description": "OTHER CARRIER CONTRACT LIMIT AMOUNT",
      "$ref": "Amount"
    },
    "totalCoinsuranceAmount": {
      "$ref": "Amount"
    },
    "totalCopaymentAmount": {
      "$ref": "Amount"
    },
    "carrierNonEligibleAmount": {
      "description": "TOTAL COORDINATION OF BENEFITS OTHER CARRIER NON-ELIGIBLE AMOUNT is The dollar amount not considered for payment for this claim under the patients other medical policy.",
      "$ref": "Amount"
    },
    "totalDeductibleAmount": {
      "$ref": "Amount"
    },
    "totalLatePaymentPenaltyInterestAmount": {
      "$ref": "Amount"
    },
    "totalNetworkDiscountAmount": {
      "$ref": "Amount"
    },
    "totalNonCoveredAmount": {
      "$ref": "Amount"
    },
    "totalPaidAmount": {
      "$ref": "Amount"
    },
    "totalPlanSavingsAmount": {
      "$ref": "Amount"
    },
    "totalRejectedAmount": {
      "$ref": "Amount"
    },
    "totalWithholdAmount": {
      "$ref": "Amount"
    },
    "totalHealthReimbursmentPaidAmount": {
      "$ref": "Amount"
    }
  }
}
*/

public class ClaimAmountSet {

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount billedChargeAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount coinsuranceAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount copaymentAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount deductibleAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount nonCoveredAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount allowedAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount withholdingAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount paidAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount hRAPaidAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount hRAAppliedAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount cDHPDeductiblePriorToHRAAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount memberResponsibilityAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount memberLiabilityAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount memberNotResponsibilityAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount providerResponsibilityAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount nonCoveredProviderResponsibilityAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount providerWriteOffAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount otherCarrierAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount planSavingsAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount uNCAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount salesTaxAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount dispensingFeeAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount ingredientCostAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount pharmacyAncillaryAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount discountAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount carrierExclusionAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalAllowedAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount carrierLiabilityAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalBilledChargeAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount carrierLimitAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalCoinsuranceAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalCopaymentAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount carrierNonEligibleAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalDeductibleAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalLatePaymentPenaltyInterestAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalNetworkDiscountAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalNonCoveredAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalPaidAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalPlanSavingsAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalRejectedAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalWithholdAmount;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Amount totalHealthReimbursmentPaidAmount;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	    amount = new com.digitalml.healthcare.foundation.shared.Amount();
	}
	public com.digitalml.healthcare.foundation.shared.Amount getBilledChargeAmount() {
		return billedChargeAmount;
	}
	
	public void setBilledChargeAmount(com.digitalml.healthcare.foundation.shared.Amount billedChargeAmount) {
		this.billedChargeAmount = billedChargeAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getCoinsuranceAmount() {
		return coinsuranceAmount;
	}
	
	public void setCoinsuranceAmount(com.digitalml.healthcare.foundation.shared.Amount coinsuranceAmount) {
		this.coinsuranceAmount = coinsuranceAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getCopaymentAmount() {
		return copaymentAmount;
	}
	
	public void setCopaymentAmount(com.digitalml.healthcare.foundation.shared.Amount copaymentAmount) {
		this.copaymentAmount = copaymentAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getDeductibleAmount() {
		return deductibleAmount;
	}
	
	public void setDeductibleAmount(com.digitalml.healthcare.foundation.shared.Amount deductibleAmount) {
		this.deductibleAmount = deductibleAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getNonCoveredAmount() {
		return nonCoveredAmount;
	}
	
	public void setNonCoveredAmount(com.digitalml.healthcare.foundation.shared.Amount nonCoveredAmount) {
		this.nonCoveredAmount = nonCoveredAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getAllowedAmount() {
		return allowedAmount;
	}
	
	public void setAllowedAmount(com.digitalml.healthcare.foundation.shared.Amount allowedAmount) {
		this.allowedAmount = allowedAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getWithholdingAmount() {
		return withholdingAmount;
	}
	
	public void setWithholdingAmount(com.digitalml.healthcare.foundation.shared.Amount withholdingAmount) {
		this.withholdingAmount = withholdingAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getPaidAmount() {
		return paidAmount;
	}
	
	public void setPaidAmount(com.digitalml.healthcare.foundation.shared.Amount paidAmount) {
		this.paidAmount = paidAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getHRAPaidAmount() {
		return hRAPaidAmount;
	}
	
	public void setHRAPaidAmount(com.digitalml.healthcare.foundation.shared.Amount hRAPaidAmount) {
		this.hRAPaidAmount = hRAPaidAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getHRAAppliedAmount() {
		return hRAAppliedAmount;
	}
	
	public void setHRAAppliedAmount(com.digitalml.healthcare.foundation.shared.Amount hRAAppliedAmount) {
		this.hRAAppliedAmount = hRAAppliedAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getCDHPDeductiblePriorToHRAAmount() {
		return cDHPDeductiblePriorToHRAAmount;
	}
	
	public void setCDHPDeductiblePriorToHRAAmount(com.digitalml.healthcare.foundation.shared.Amount cDHPDeductiblePriorToHRAAmount) {
		this.cDHPDeductiblePriorToHRAAmount = cDHPDeductiblePriorToHRAAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getMemberResponsibilityAmount() {
		return memberResponsibilityAmount;
	}
	
	public void setMemberResponsibilityAmount(com.digitalml.healthcare.foundation.shared.Amount memberResponsibilityAmount) {
		this.memberResponsibilityAmount = memberResponsibilityAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getMemberLiabilityAmount() {
		return memberLiabilityAmount;
	}
	
	public void setMemberLiabilityAmount(com.digitalml.healthcare.foundation.shared.Amount memberLiabilityAmount) {
		this.memberLiabilityAmount = memberLiabilityAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getMemberNotResponsibilityAmount() {
		return memberNotResponsibilityAmount;
	}
	
	public void setMemberNotResponsibilityAmount(com.digitalml.healthcare.foundation.shared.Amount memberNotResponsibilityAmount) {
		this.memberNotResponsibilityAmount = memberNotResponsibilityAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getProviderResponsibilityAmount() {
		return providerResponsibilityAmount;
	}
	
	public void setProviderResponsibilityAmount(com.digitalml.healthcare.foundation.shared.Amount providerResponsibilityAmount) {
		this.providerResponsibilityAmount = providerResponsibilityAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getNonCoveredProviderResponsibilityAmount() {
		return nonCoveredProviderResponsibilityAmount;
	}
	
	public void setNonCoveredProviderResponsibilityAmount(com.digitalml.healthcare.foundation.shared.Amount nonCoveredProviderResponsibilityAmount) {
		this.nonCoveredProviderResponsibilityAmount = nonCoveredProviderResponsibilityAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getProviderWriteOffAmount() {
		return providerWriteOffAmount;
	}
	
	public void setProviderWriteOffAmount(com.digitalml.healthcare.foundation.shared.Amount providerWriteOffAmount) {
		this.providerWriteOffAmount = providerWriteOffAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getOtherCarrierAmount() {
		return otherCarrierAmount;
	}
	
	public void setOtherCarrierAmount(com.digitalml.healthcare.foundation.shared.Amount otherCarrierAmount) {
		this.otherCarrierAmount = otherCarrierAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getPlanSavingsAmount() {
		return planSavingsAmount;
	}
	
	public void setPlanSavingsAmount(com.digitalml.healthcare.foundation.shared.Amount planSavingsAmount) {
		this.planSavingsAmount = planSavingsAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getUNCAmount() {
		return uNCAmount;
	}
	
	public void setUNCAmount(com.digitalml.healthcare.foundation.shared.Amount uNCAmount) {
		this.uNCAmount = uNCAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getSalesTaxAmount() {
		return salesTaxAmount;
	}
	
	public void setSalesTaxAmount(com.digitalml.healthcare.foundation.shared.Amount salesTaxAmount) {
		this.salesTaxAmount = salesTaxAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getDispensingFeeAmount() {
		return dispensingFeeAmount;
	}
	
	public void setDispensingFeeAmount(com.digitalml.healthcare.foundation.shared.Amount dispensingFeeAmount) {
		this.dispensingFeeAmount = dispensingFeeAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getIngredientCostAmount() {
		return ingredientCostAmount;
	}
	
	public void setIngredientCostAmount(com.digitalml.healthcare.foundation.shared.Amount ingredientCostAmount) {
		this.ingredientCostAmount = ingredientCostAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getPharmacyAncillaryAmount() {
		return pharmacyAncillaryAmount;
	}
	
	public void setPharmacyAncillaryAmount(com.digitalml.healthcare.foundation.shared.Amount pharmacyAncillaryAmount) {
		this.pharmacyAncillaryAmount = pharmacyAncillaryAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getDiscountAmount() {
		return discountAmount;
	}
	
	public void setDiscountAmount(com.digitalml.healthcare.foundation.shared.Amount discountAmount) {
		this.discountAmount = discountAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getCarrierExclusionAmount() {
		return carrierExclusionAmount;
	}
	
	public void setCarrierExclusionAmount(com.digitalml.healthcare.foundation.shared.Amount carrierExclusionAmount) {
		this.carrierExclusionAmount = carrierExclusionAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalAllowedAmount() {
		return totalAllowedAmount;
	}
	
	public void setTotalAllowedAmount(com.digitalml.healthcare.foundation.shared.Amount totalAllowedAmount) {
		this.totalAllowedAmount = totalAllowedAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getCarrierLiabilityAmount() {
		return carrierLiabilityAmount;
	}
	
	public void setCarrierLiabilityAmount(com.digitalml.healthcare.foundation.shared.Amount carrierLiabilityAmount) {
		this.carrierLiabilityAmount = carrierLiabilityAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalBilledChargeAmount() {
		return totalBilledChargeAmount;
	}
	
	public void setTotalBilledChargeAmount(com.digitalml.healthcare.foundation.shared.Amount totalBilledChargeAmount) {
		this.totalBilledChargeAmount = totalBilledChargeAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getCarrierLimitAmount() {
		return carrierLimitAmount;
	}
	
	public void setCarrierLimitAmount(com.digitalml.healthcare.foundation.shared.Amount carrierLimitAmount) {
		this.carrierLimitAmount = carrierLimitAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalCoinsuranceAmount() {
		return totalCoinsuranceAmount;
	}
	
	public void setTotalCoinsuranceAmount(com.digitalml.healthcare.foundation.shared.Amount totalCoinsuranceAmount) {
		this.totalCoinsuranceAmount = totalCoinsuranceAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalCopaymentAmount() {
		return totalCopaymentAmount;
	}
	
	public void setTotalCopaymentAmount(com.digitalml.healthcare.foundation.shared.Amount totalCopaymentAmount) {
		this.totalCopaymentAmount = totalCopaymentAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getCarrierNonEligibleAmount() {
		return carrierNonEligibleAmount;
	}
	
	public void setCarrierNonEligibleAmount(com.digitalml.healthcare.foundation.shared.Amount carrierNonEligibleAmount) {
		this.carrierNonEligibleAmount = carrierNonEligibleAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalDeductibleAmount() {
		return totalDeductibleAmount;
	}
	
	public void setTotalDeductibleAmount(com.digitalml.healthcare.foundation.shared.Amount totalDeductibleAmount) {
		this.totalDeductibleAmount = totalDeductibleAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalLatePaymentPenaltyInterestAmount() {
		return totalLatePaymentPenaltyInterestAmount;
	}
	
	public void setTotalLatePaymentPenaltyInterestAmount(com.digitalml.healthcare.foundation.shared.Amount totalLatePaymentPenaltyInterestAmount) {
		this.totalLatePaymentPenaltyInterestAmount = totalLatePaymentPenaltyInterestAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalNetworkDiscountAmount() {
		return totalNetworkDiscountAmount;
	}
	
	public void setTotalNetworkDiscountAmount(com.digitalml.healthcare.foundation.shared.Amount totalNetworkDiscountAmount) {
		this.totalNetworkDiscountAmount = totalNetworkDiscountAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalNonCoveredAmount() {
		return totalNonCoveredAmount;
	}
	
	public void setTotalNonCoveredAmount(com.digitalml.healthcare.foundation.shared.Amount totalNonCoveredAmount) {
		this.totalNonCoveredAmount = totalNonCoveredAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalPaidAmount() {
		return totalPaidAmount;
	}
	
	public void setTotalPaidAmount(com.digitalml.healthcare.foundation.shared.Amount totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalPlanSavingsAmount() {
		return totalPlanSavingsAmount;
	}
	
	public void setTotalPlanSavingsAmount(com.digitalml.healthcare.foundation.shared.Amount totalPlanSavingsAmount) {
		this.totalPlanSavingsAmount = totalPlanSavingsAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalRejectedAmount() {
		return totalRejectedAmount;
	}
	
	public void setTotalRejectedAmount(com.digitalml.healthcare.foundation.shared.Amount totalRejectedAmount) {
		this.totalRejectedAmount = totalRejectedAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalWithholdAmount() {
		return totalWithholdAmount;
	}
	
	public void setTotalWithholdAmount(com.digitalml.healthcare.foundation.shared.Amount totalWithholdAmount) {
		this.totalWithholdAmount = totalWithholdAmount;
	}
	public com.digitalml.healthcare.foundation.shared.Amount getTotalHealthReimbursmentPaidAmount() {
		return totalHealthReimbursmentPaidAmount;
	}
	
	public void setTotalHealthReimbursmentPaidAmount(com.digitalml.healthcare.foundation.shared.Amount totalHealthReimbursmentPaidAmount) {
		this.totalHealthReimbursmentPaidAmount = totalHealthReimbursmentPaidAmount;
	}
}