package com.digitalml.healthcare.foundation.shared;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for PersonName:
{
  "type": "object",
  "properties": {
    "firstName": {
      "type": "string"
    },
    "lastName": {
      "type": "string"
    },
    "middleNames": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "suffix": {
      "type": "string"
    },
    "prefix": {
      "type": "string"
    },
    "fullName": {
      "type": "string"
    },
    "maidenName": {
      "type": "string"
    },
    "nickname": {
      "type": "string"
    }
  }
}
*/

public class PersonName {

	@Size(max=1)
	private String firstName;

	@Size(max=1)
	private String lastName;

	@Size(max=1)
	private List<String> middleNames;

	@Size(max=1)
	private String suffix;

	@Size(max=1)
	private String prefix;

	@Size(max=1)
	private String fullName;

	@Size(max=1)
	private String maidenName;

	@Size(max=1)
	private String nickname;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    firstName = null;
	    lastName = null;
	    middleNames = new ArrayList<String>();
	    suffix = null;
	    prefix = null;
	    fullName = null;
	    maidenName = null;
	    nickname = null;
	}
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<String> getMiddleNames() {
		return middleNames;
	}
	
	public void setMiddleNames(List<String> middleNames) {
		this.middleNames = middleNames;
	}
	public String getSuffix() {
		return suffix;
	}
	
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getPrefix() {
		return prefix;
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getMaidenName() {
		return maidenName;
	}
	
	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}
	public String getNickname() {
		return nickname;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
}