package com.digitalml.healthcare.foundation.shared;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Person:
{
  "type": "object",
  "properties": {
    "name": {
      "$ref": "PersonName"
    },
    "dateOfBirth": {
      "type": "string",
      "format": "date"
    },
    "genderCode": {
      "type": "string"
    }
  }
}
*/

public class Person {

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.PersonName name;

	@Size(max=1)
	private Date dateOfBirth;

	@Size(max=1)
	private String genderCode;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    personName = new com.digitalml.healthcare.foundation.shared.PersonName();
	    
	    genderCode = null;
	}
	public com.digitalml.healthcare.foundation.shared.PersonName getName() {
		return name;
	}
	
	public void setName(com.digitalml.healthcare.foundation.shared.PersonName name) {
		this.name = name;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGenderCode() {
		return genderCode;
	}
	
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
}