package com.digitalml.rest.resources.codegentest.service.Claim;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import com.digitalml.rest.resources.codegentest.service.ClaimService;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;

/**
 * Sandbox implementation for: Claim
 * The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical
information, regarding the provision of healthcare services with payors an firms which provide data analytics. The
primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors,
insurers and national health programs, for treatment payment planning and reimbursement.
 *
 * @author admin
 * @version 1.0
 *
 */

public class ClaimServiceSandboxImpl extends ClaimService {
	

    public GetASpecificClaimCurrentStateDTO getaspecificclaimUseCaseStep1(GetASpecificClaimCurrentStateDTO currentState) {
    

        GetASpecificClaimReturnStatusDTO returnStatus = new GetASpecificClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificClaimCurrentStateDTO getaspecificclaimUseCaseStep2(GetASpecificClaimCurrentStateDTO currentState) {
    

        GetASpecificClaimReturnStatusDTO returnStatus = new GetASpecificClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificClaimCurrentStateDTO getaspecificclaimUseCaseStep3(GetASpecificClaimCurrentStateDTO currentState) {
    

        GetASpecificClaimReturnStatusDTO returnStatus = new GetASpecificClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificClaimCurrentStateDTO getaspecificclaimUseCaseStep4(GetASpecificClaimCurrentStateDTO currentState) {
    

        GetASpecificClaimReturnStatusDTO returnStatus = new GetASpecificClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificClaimCurrentStateDTO getaspecificclaimUseCaseStep5(GetASpecificClaimCurrentStateDTO currentState) {
    

        GetASpecificClaimReturnStatusDTO returnStatus = new GetASpecificClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificClaimCurrentStateDTO getaspecificclaimUseCaseStep6(GetASpecificClaimCurrentStateDTO currentState) {
    

        GetASpecificClaimReturnStatusDTO returnStatus = new GetASpecificClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public FindClaimCurrentStateDTO findclaimUseCaseStep1(FindClaimCurrentStateDTO currentState) {
    

        FindClaimReturnStatusDTO returnStatus = new FindClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindClaimCurrentStateDTO findclaimUseCaseStep2(FindClaimCurrentStateDTO currentState) {
    

        FindClaimReturnStatusDTO returnStatus = new FindClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindClaimCurrentStateDTO findclaimUseCaseStep3(FindClaimCurrentStateDTO currentState) {
    

        FindClaimReturnStatusDTO returnStatus = new FindClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindClaimCurrentStateDTO findclaimUseCaseStep4(FindClaimCurrentStateDTO currentState) {
    

        FindClaimReturnStatusDTO returnStatus = new FindClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindClaimCurrentStateDTO findclaimUseCaseStep5(FindClaimCurrentStateDTO currentState) {
    

        FindClaimReturnStatusDTO returnStatus = new FindClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindClaimCurrentStateDTO findclaimUseCaseStep6(FindClaimCurrentStateDTO currentState) {
    

        FindClaimReturnStatusDTO returnStatus = new FindClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public UpdateClaimCurrentStateDTO updateclaimUseCaseStep1(UpdateClaimCurrentStateDTO currentState) {
    

        UpdateClaimReturnStatusDTO returnStatus = new UpdateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateClaimCurrentStateDTO updateclaimUseCaseStep2(UpdateClaimCurrentStateDTO currentState) {
    

        UpdateClaimReturnStatusDTO returnStatus = new UpdateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateClaimCurrentStateDTO updateclaimUseCaseStep3(UpdateClaimCurrentStateDTO currentState) {
    

        UpdateClaimReturnStatusDTO returnStatus = new UpdateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateClaimCurrentStateDTO updateclaimUseCaseStep4(UpdateClaimCurrentStateDTO currentState) {
    

        UpdateClaimReturnStatusDTO returnStatus = new UpdateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateClaimCurrentStateDTO updateclaimUseCaseStep5(UpdateClaimCurrentStateDTO currentState) {
    

        UpdateClaimReturnStatusDTO returnStatus = new UpdateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateClaimCurrentStateDTO updateclaimUseCaseStep6(UpdateClaimCurrentStateDTO currentState) {
    

        UpdateClaimReturnStatusDTO returnStatus = new UpdateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateClaimCurrentStateDTO updateclaimUseCaseStep7(UpdateClaimCurrentStateDTO currentState) {
    

        UpdateClaimReturnStatusDTO returnStatus = new UpdateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public ReplaceClaimCurrentStateDTO replaceclaimUseCaseStep1(ReplaceClaimCurrentStateDTO currentState) {
    

        ReplaceClaimReturnStatusDTO returnStatus = new ReplaceClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceClaimCurrentStateDTO replaceclaimUseCaseStep2(ReplaceClaimCurrentStateDTO currentState) {
    

        ReplaceClaimReturnStatusDTO returnStatus = new ReplaceClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceClaimCurrentStateDTO replaceclaimUseCaseStep3(ReplaceClaimCurrentStateDTO currentState) {
    

        ReplaceClaimReturnStatusDTO returnStatus = new ReplaceClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceClaimCurrentStateDTO replaceclaimUseCaseStep4(ReplaceClaimCurrentStateDTO currentState) {
    

        ReplaceClaimReturnStatusDTO returnStatus = new ReplaceClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceClaimCurrentStateDTO replaceclaimUseCaseStep5(ReplaceClaimCurrentStateDTO currentState) {
    

        ReplaceClaimReturnStatusDTO returnStatus = new ReplaceClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceClaimCurrentStateDTO replaceclaimUseCaseStep6(ReplaceClaimCurrentStateDTO currentState) {
    

        ReplaceClaimReturnStatusDTO returnStatus = new ReplaceClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceClaimCurrentStateDTO replaceclaimUseCaseStep7(ReplaceClaimCurrentStateDTO currentState) {
    

        ReplaceClaimReturnStatusDTO returnStatus = new ReplaceClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public CreateClaimCurrentStateDTO createclaimUseCaseStep1(CreateClaimCurrentStateDTO currentState) {
    

        CreateClaimReturnStatusDTO returnStatus = new CreateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateClaimCurrentStateDTO createclaimUseCaseStep2(CreateClaimCurrentStateDTO currentState) {
    

        CreateClaimReturnStatusDTO returnStatus = new CreateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateClaimCurrentStateDTO createclaimUseCaseStep3(CreateClaimCurrentStateDTO currentState) {
    

        CreateClaimReturnStatusDTO returnStatus = new CreateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateClaimCurrentStateDTO createclaimUseCaseStep4(CreateClaimCurrentStateDTO currentState) {
    

        CreateClaimReturnStatusDTO returnStatus = new CreateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateClaimCurrentStateDTO createclaimUseCaseStep5(CreateClaimCurrentStateDTO currentState) {
    

        CreateClaimReturnStatusDTO returnStatus = new CreateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateClaimCurrentStateDTO createclaimUseCaseStep6(CreateClaimCurrentStateDTO currentState) {
    

        CreateClaimReturnStatusDTO returnStatus = new CreateClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public DeleteClaimCurrentStateDTO deleteclaimUseCaseStep1(DeleteClaimCurrentStateDTO currentState) {
    

        DeleteClaimReturnStatusDTO returnStatus = new DeleteClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteClaimCurrentStateDTO deleteclaimUseCaseStep2(DeleteClaimCurrentStateDTO currentState) {
    

        DeleteClaimReturnStatusDTO returnStatus = new DeleteClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteClaimCurrentStateDTO deleteclaimUseCaseStep3(DeleteClaimCurrentStateDTO currentState) {
    

        DeleteClaimReturnStatusDTO returnStatus = new DeleteClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteClaimCurrentStateDTO deleteclaimUseCaseStep4(DeleteClaimCurrentStateDTO currentState) {
    

        DeleteClaimReturnStatusDTO returnStatus = new DeleteClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteClaimCurrentStateDTO deleteclaimUseCaseStep5(DeleteClaimCurrentStateDTO currentState) {
    

        DeleteClaimReturnStatusDTO returnStatus = new DeleteClaimReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical information, regarding the provision of healthcare services with payors an firms which provide data analytics. The primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors, insurers and national health programs, for treatment payment planning and reimbursement.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = ClaimService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}