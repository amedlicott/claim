package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "took",
    "total",
    "claim"
  ],
  "type": "object",
  "properties": {
    "claim": {
      "$ref": "Claim"
    },
    "total": {
      "description": "Total objects matching search (may be different than the actual number returned because of paging)",
      "type": "integer",
      "format": "int64"
    },
    "took": {
      "description": "Time taken for search",
      "type": "string"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private com.digitalml.healthcare.financial.claim.Claim claim;

	@Size(max=1)
	@NotNull
	private Integer total;

	@Size(max=1)
	@NotNull
	private String took;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    claim = new com.digitalml.healthcare.financial.claim.Claim();
	    
	    took = org.apache.commons.lang3.StringUtils.EMPTY;
	}
	public com.digitalml.healthcare.financial.claim.Claim getClaim() {
		return claim;
	}
	
	public void setClaim(com.digitalml.healthcare.financial.claim.Claim claim) {
		this.claim = claim;
	}
	public Integer getTotal() {
		return total;
	}
	
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getTook() {
		return took;
	}
	
	public void setTook(String took) {
		this.took = took;
	}
}