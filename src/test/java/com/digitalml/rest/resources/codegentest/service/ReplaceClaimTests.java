package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Claim.ClaimServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ClaimService.ReplaceClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimService.ReplaceClaimReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class ReplaceClaimTests {

	@Test
	public void testOperationReplaceClaimBasicMapping()  {
		ClaimServiceDefaultImpl serviceDefaultImpl = new ClaimServiceDefaultImpl();
		ReplaceClaimInputParametersDTO inputs = new ReplaceClaimInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		ReplaceClaimReturnDTO returnValue = serviceDefaultImpl.replaceclaim(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}