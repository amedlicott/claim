package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Claim.ClaimServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ClaimService.GetASpecificClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimService.GetASpecificClaimReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificClaimTests {

	@Test
	public void testOperationGetASpecificClaimBasicMapping()  {
		ClaimServiceDefaultImpl serviceDefaultImpl = new ClaimServiceDefaultImpl();
		GetASpecificClaimInputParametersDTO inputs = new GetASpecificClaimInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificClaimReturnDTO returnValue = serviceDefaultImpl.getaspecificclaim(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}