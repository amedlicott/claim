package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Claim.ClaimServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ClaimService.DeleteClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimService.DeleteClaimReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeleteClaimTests {

	@Test
	public void testOperationDeleteClaimBasicMapping()  {
		ClaimServiceDefaultImpl serviceDefaultImpl = new ClaimServiceDefaultImpl();
		DeleteClaimInputParametersDTO inputs = new DeleteClaimInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		DeleteClaimReturnDTO returnValue = serviceDefaultImpl.deleteclaim(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}