package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class ClaimTests {

	@Test
	public void testResourceInitialisation() {
		ClaimResource resource = new ClaimResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetASpecificClaimNoSecurity() {
		ClaimResource resource = new ClaimResource();
		resource.setSecurityContext(null);

		Response response = resource.getaspecificclaim(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationFindClaimNoSecurity() {
		ClaimResource resource = new ClaimResource();
		resource.setSecurityContext(null);

		Response response = resource.findclaim(0, 0);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateClaimNoSecurity() {
		ClaimResource resource = new ClaimResource();
		resource.setSecurityContext(null);

		Response response = resource.updateclaim(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationReplaceClaimNoSecurity() {
		ClaimResource resource = new ClaimResource();
		resource.setSecurityContext(null);

		Response response = resource.replaceclaim(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateClaimNoSecurity() {
		ClaimResource resource = new ClaimResource();
		resource.setSecurityContext(null);

		Response response = resource.createclaim(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationDeleteClaimNoSecurity() {
		ClaimResource resource = new ClaimResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteclaim(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}