package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Claim.ClaimServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ClaimService.FindClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimService.FindClaimReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindClaimTests {

	@Test
	public void testOperationFindClaimBasicMapping()  {
		ClaimServiceDefaultImpl serviceDefaultImpl = new ClaimServiceDefaultImpl();
		FindClaimInputParametersDTO inputs = new FindClaimInputParametersDTO();
		inputs.setOffset(0);
		inputs.setPagesize(0);
		FindClaimReturnDTO returnValue = serviceDefaultImpl.findclaim(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}